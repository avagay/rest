package com.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;

import static com.domain.Cache.addInCache;
import static com.domain.Cache.getCacheByName;
import static com.domain.Constants.*;

@Service
public class GeoService {


    private String readAll(Reader rd) throws IOException {

        StringBuilder sb = new StringBuilder();

        int cp;

        while ((cp = rd.read()) != -1) {

            sb.append((char) cp);
        }

        return sb.toString();
    }


    private JSONObject readJsonFromUrl(String myUrl) {

        JSONArray jsonArray = new JSONArray();

        try {

            InputStream inputStream = new URL(myUrl).openStream();

            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(inputStream, Charset.forName(UTF_8)));

            String text = readAll(bufferedReader);

            jsonArray = new JSONArray(text);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonArray.getJSONObject(0);
    }

    private JSONArray findCentreCoordinates(JSONArray arJSON) {

        double sumX = 0;

        double sumY = 0;

        int length = arJSON.length();

        for (int i = 0; i < length; i++) {

            sumX += arJSON.getJSONArray(i).getDouble(0);

            sumY += arJSON.getJSONArray(i).getDouble(1);

        }

        double centreX = sumX / length;

        double centreY = sumY / length;

        JSONArray centreArray = new JSONArray();

        centreArray.put(centreX);

        centreArray.put(centreY);

        return centreArray;
    }


    private JSONArray findArray(JSONObject json) {

        JSONObject geojson = (JSONObject) json.get(GEOJSON);

        JSONArray arJSON = geojson.getJSONArray(COORDINATES);

        int indexMax = 0;

        for (int i = 0; i < arJSON.length(); i++) {

            if (arJSON.getJSONArray(indexMax).getJSONArray(0).length()
                    < arJSON.getJSONArray(i).getJSONArray(0).length()) {

                indexMax = i;

            }

        }

        return arJSON.getJSONArray(indexMax).getJSONArray(0);

    }


    private JSONObject getJson(String name, String urlType) {

        String urlMap = "https://nominatim.openstreetmap.org/search?" + urlType +
                name.replace(" ", "%20") + "&country=russia&format=json&polygon_geojson=1";

        return readJsonFromUrl(urlMap);
    }


    public JSONArray buildOutJson(String name, String urlType) {

        JSONArray array = getCacheByName(name);

        if (Objects.nonNull(array)) {
            return array;
        }

        array = new JSONArray();

        JSONObject json = getJson(name, urlType);

        JSONArray arJSON = findArray(json);

        JSONArray centreCoordinates = findCentreCoordinates(arJSON);

        JSONObject jsonObject = new JSONObject();

        jsonObject.put(CENTRE, centreCoordinates);

        jsonObject.put(ARRAY, arJSON);

        array.put(jsonObject);

        addInCache(name, array);

        return array;
    }


    public String decoder(String name, String encoding) {

        byte[] bytes = new byte[0];
        String decoderName = null;
        try {
            bytes = name.getBytes(encoding);

            decoderName = new String(bytes, WINDOWS_1251);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return decoderName;
    }

}





