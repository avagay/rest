package com.domain;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

public class Cache {

    private static Map<String, JSONArray> cacheMap = new HashMap<>();


    public static void addInCache(String name, JSONArray jsonArray) {
        cacheMap.put(name, jsonArray);
    }

    public static JSONArray getCacheByName(String name) {
        return cacheMap.get(name);
    }
}
