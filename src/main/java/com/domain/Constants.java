package com.domain;

public class Constants {

    public static final String ISO_8859_1 = "ISO-8859-1";
    public static final String WINDOWS_1251 = "Windows-1251";
    public static final String UTF_8 = "UTF-8";
    public static final String STATE = "state=";
    public static final String Q = "q=";
    public static final String GEOJSON = "geojson";
    public static final String COORDINATES = "coordinates";
    public static final String CENTRE = "centre";
    public static final String ARRAY = "array";

}
