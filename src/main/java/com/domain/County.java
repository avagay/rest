package com.domain;

public class County {

    private String name;


    public County() {
    }

    public County(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
