

package com.controller;


import com.domain.County;
import com.domain.Region;
import com.service.GeoService;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.domain.Constants.*;


@RestController
@RequestMapping(value = "/geo")
public class GeoController {

    @Autowired
    private GeoService geoService;


    @GetMapping(value = "/region/{name}")
    public String getRegion(@PathVariable String name) {

        JSONArray jsonArray = geoService.buildOutJson(geoService.decoder(name, ISO_8859_1), STATE);

        return jsonArray.toString();
    }

    @GetMapping(value = "/county/{name}")
    public String getCounty(@PathVariable String name) {

        JSONArray jsonArray = geoService.buildOutJson(geoService.decoder(name, ISO_8859_1), Q);

        return jsonArray.toString();
    }


    @PostMapping(value = "/region")
    public String postRegion(@RequestBody Region region) {

        JSONArray jsonArray = geoService.buildOutJson(geoService.decoder(region.getName(), UTF_8), STATE);

        return jsonArray.toString();

    }

    @PostMapping(value = "/county")
    public String postCounty(@RequestBody County county) {

        JSONArray jsonArray = geoService.buildOutJson(geoService.decoder(county.getName(), UTF_8), Q);

        return jsonArray.toString();
    }


}






